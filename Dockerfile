FROM node:18.14.2-alpine

WORKDIR /var/app

COPY ./package.json .
COPY ./yarn.lock .

RUN yarn install

COPY ./src ./src
COPY ./public ./public

COPY ./tsconfig.config.json .
COPY ./tsconfig.json .

COPY ./env.d.ts .
COPY ./vite.config.ts .

COPY index.html .

CMD ["yarn", "run", "dev", "--host"]
