import { z } from 'zod'
import type { Either } from 'fp-ts/Either'
import { left, right } from 'fp-ts/Either'

const CardImage = z.object({
  url: z.string(),
  cacheImageUrl: z.nullable(z.string()),
  h: z.number(),
  w: z.number()
})

const AlternativeVideos = z.object({
  quality: z.string(),
  url: z.string()
})

const Video = z.object({
  title: z.string(),
  type: z.string(),
  url: z.string(),
  alternatives: z.nullable(z.array(AlternativeVideos))
})

const Person = z.object({
  name: z.string()
})

const ViewingWindow = z.object({
  startDate: z.string().transform((res) => new Date(res)),
  wayToWatch: z.string(),
  endDate: z.string().transform((res) => new Date(res))
})

export const Movie = z.object({
  body: z.string(),
  cardImages: z.array(CardImage),
  cast: z.array(Person),
  cert: z.string(),
  class: z.string(),
  directors: z.array(Person),
  duration: z.number(),
  genres: z.nullable(z.array(z.string())),
  headline: z.string(),
  id: z.string(),
  keyArtImages: z.array(CardImage),
  lastUpdated: z.string().transform((res) => new Date(res)),
  quote: z.nullable(z.string()),
  rating: z.nullable(z.number()),
  reviewAuthor: z.nullable(z.string()),
  skyGoId: z.nullable(z.string()),
  skyGoUrl: z.nullable(z.string()),
  sum: z.string(),
  synopsis: z.string(),
  url: z.string(),
  videos: z.nullable(z.array(Video)),
  viewingWindow: z.nullable(ViewingWindow),
  year: z.string()
})

export type MovieType = z.infer<typeof Movie>

const PagedResponse = z.object({
  data: z.any(),
  totalCount: z.number(),
  pageSize: z.number(),
  pageIndex: z.number()
})

export type PagedResponseType<T> = {
  data: T[]
  totalCount: number
  pageSize: number
  pageIndex: number
}

const serviceURL = import.meta.env.VITE_SCRAPPER_SERVICE_URL

export const getAllMovies = async (
  pageSize = 20,
  pageIndex = 1
): Promise<Either<Error, PagedResponseType<MovieType>>> => {
  const url = new URL(`${serviceURL}/api/external/Movies/`)
  url.searchParams.append('pageSize', pageSize.toString())
  url.searchParams.append('pageIndex', pageIndex.toString())

  return fetch(url)
    .then((res) => res.json())
    .then((res) => PagedResponse.parseAsync(res))
    .then(async (res) => right(
      {
        data: await z.array(Movie).parseAsync(res.data),
        totalCount: res.totalCount,
        pageSize: res.pageSize,
        pageIndex: res.pageIndex
      }
    ))
    .catch((err) => left(err))
}

export const downloadFile = async (fileUrl: string) => {
  const url = new URL(`${serviceURL}/api/external/Movies/download-artwork/${fileUrl}`)
  return fetch(url)
    .then(res => res.blob())
}
